# README #

This is the repository for the RNA editing manuscript. Filed generated are designed to form a Docker Repository Bjorklund/rna-edit

The Raw sequence files can be downloaded locally and then mounted into the Docker container at launch. This enabled hig flexibility while retaining a small Docker footprint.




### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Docker image is built using 

```
#!shell

docker build -t bjorklund/rna_edit .

docker login
docker push bjorklund/rna_edit
```

* Configuration
* Dependencies
* Database configuration
* How to run tests


```
#!shell

docker run -d -v /path-to_sequenceFolder:/home/rstudio/seqFiles -i -p 9797:8787 --name rna-edit bjorklund/rna_edit

```

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
