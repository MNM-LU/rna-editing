

processing file: DNA_LibMapping.spin.Rmd
  |                                                                         |                                                                 |   0%  |                                                                         |..                                                               |   3%
  ordinary text without R code

  |                                                                         |....                                                             |   6%
label: unnamed-chunk-1
  |                                                                         |......                                                           |   9%
label: setup (with options) 
List of 1
 $ include: logi FALSE

  |                                                                         |........                                                         |  12%
  ordinary text without R code

  |                                                                         |..........                                                       |  16%
label: unnamed-chunk-2
  |                                                                         |............                                                     |  19%
  ordinary text without R code

  |                                                                         |..............                                                   |  22%
label: unnamed-chunk-3
  |                                                                         |................                                                 |  25%
  ordinary text without R code

  |                                                                         |..................                                               |  28%
label: unnamed-chunk-4
  |                                                                         |....................                                             |  31%
  ordinary text without R code

  |                                                                         |......................                                           |  34%
label: Extracting subset.......
  |                                                                         |........................                                         |  38%
  ordinary text without R code

  |                                                                         |..........................                                       |  41%
label: Extracting barcodes.......
  |                                                                         |............................                                     |  44%
  ordinary text without R code

  |                                                                         |..............................                                   |  47%
label: Reducing barcodes.......
  |                                                                         |................................                                 |  50%
  ordinary text without R code

  |                                                                         |...................................                              |  53%
label: Extracting fragments.......
  |                                                                         |.....................................                            |  56%
  ordinary text without R code

  |                                                                         |.......................................                          |  59%
label: Align to reference...
[samopen] SAM header is present: 1 sequences.
[samopen] SAM header is present: 1 sequences.
  |                                                                         |.........................................                        |  62%
  ordinary text without R code

  |                                                                         |...........................................                      |  66%
label: Generating summary table.......
  |                                                                         |.............................................                    |  69%
  ordinary text without R code

  |                                                                         |...............................................                  |  72%
label: GeneratingPlots
cropping DNA_LibMapping_files/figure-latex/GeneratingPlots-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/GeneratingPlots-1.pdf'.
  |                                                                         |.................................................                |  75%
  ordinary text without R code

  |                                                                         |...................................................              |  78%
label: beanplot
cropping DNA_LibMapping_files/figure-latex/beanplot-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/beanplot-1.pdf'.
cropping DNA_LibMapping_files/figure-latex/beanplot-2.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/beanplot-2.pdf'.
cropping DNA_LibMapping_files/figure-latex/beanplot-3.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/beanplot-3.pdf'.
  |                                                                         |.....................................................            |  81%
  ordinary text without R code

  |                                                                         |.......................................................          |  84%
label: unnamed-chunk-5
cropping DNA_LibMapping_files/figure-latex/unnamed-chunk-5-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/unnamed-chunk-5-1.pdf'.
  |                                                                         |.........................................................        |  88%
  ordinary text without R code

  |                                                                         |...........................................................      |  91%
label: fragmentDistribution
cropping DNA_LibMapping_files/figure-latex/fragmentDistribution-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/fragmentDistribution-1.pdf'.
  |                                                                         |.............................................................    |  94%
  ordinary text without R code

  |                                                                         |...............................................................  |  97%
label: uniqueFragments
cropping DNA_LibMapping_files/figure-latex/uniqueFragments-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/uniqueFragments-1.pdf'.
cropping DNA_LibMapping_files/figure-latex/uniqueFragments-2.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `DNA_LibMapping_files/figure-latex/uniqueFragments-2.pdf'.
  |                                                                         |.................................................................| 100%
  ordinary text without R code


output file: DNA_LibMapping.knit.md

/usr/bin/pandoc +RTS -K512m -RTS DNA_LibMapping.utf8.md --to latex --from markdown+autolink_bare_uris+ascii_identifiers+tex_math_single_backslash --output DNA_LibMapping.pdf --template /home/lcadmin/R/x86_64-pc-linux-gnu-library/3.2/rmarkdown/rmd/latex/default-1.15.2.tex --highlight-style tango --latex-engine pdflatex --variable graphics=yes 

Output created: DNA_LibMapping.pdf
