

processing file: mRNA-spliceCount.spin.Rmd
  |                                                                         |                                                                 |   0%  |                                                                         |...                                                              |   5%
  ordinary text without R code

  |                                                                         |......                                                           |   9%
label: unnamed-chunk-1
  |                                                                         |.........                                                        |  14%
label: setup (with options) 
List of 1
 $ include: logi FALSE

  |                                                                         |............                                                     |  18%
  ordinary text without R code

  |                                                                         |...............                                                  |  23%
label: unnamed-chunk-2
  |                                                                         |..................                                               |  27%
  ordinary text without R code

  |                                                                         |.....................                                            |  32%
label: unnamed-chunk-3
  |                                                                         |........................                                         |  36%
  ordinary text without R code

  |                                                                         |...........................                                      |  41%
label: unnamed-chunk-4
  |                                                                         |..............................                                   |  45%
  ordinary text without R code

  |                                                                         |................................                                 |  50%
label: Extracting subset.......
  |                                                                         |...................................                              |  55%
  ordinary text without R code

  |                                                                         |......................................                           |  59%
label: Selecting valid mRNA.......
  |                                                                         |.........................................                        |  64%
  ordinary text without R code

  |                                                                         |............................................                     |  68%
label: Extracting barcodes.......
  |                                                                         |...............................................                  |  73%
  ordinary text without R code

  |                                                                         |..................................................               |  77%
label: Generating summary table.......
  |                                                                         |.....................................................            |  82%
  ordinary text without R code

  |                                                                         |........................................................         |  86%
label: uniqueFragments
cropping mRNA-spliceCount_files/figure-latex/uniqueFragments-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `mRNA-spliceCount_files/figure-latex/uniqueFragments-1.pdf'.
  |                                                                         |...........................................................      |  91%
  ordinary text without R code

  |                                                                         |..............................................................   |  95%
label: CoveragePlots
cropping mRNA-spliceCount_files/figure-latex/CoveragePlots-1.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `mRNA-spliceCount_files/figure-latex/CoveragePlots-1.pdf'.
cropping mRNA-spliceCount_files/figure-latex/CoveragePlots-2.pdf
PDFCROP 1.38, 2012/11/02 - Copyright (c) 2002-2012 by Heiko Oberdiek.
==> 1 page written on `mRNA-spliceCount_files/figure-latex/CoveragePlots-2.pdf'.
  |                                                                         |.................................................................| 100%
  ordinary text without R code


output file: mRNA-spliceCount.knit.md

/usr/bin/pandoc +RTS -K512m -RTS mRNA-spliceCount.utf8.md --to latex --from markdown+autolink_bare_uris+ascii_identifiers+tex_math_single_backslash --output mRNA-spliceCount.pdf --template /home/lcadmin/R/x86_64-pc-linux-gnu-library/3.2/rmarkdown/rmd/latex/default-1.15.2.tex --highlight-style tango --latex-engine pdflatex --variable graphics=yes 

Output created: mRNA-spliceCount.pdf
