buildFragLengthTable <- function(bam, binning=1) #Returns a table with identified consensus barcodes
{


library(BSgenome)
require(Rsamtools)
library(rtracklayer)
library(GenomicFeatures)
require(GenomicAlignments)
library(Gviz)
require(parallel)
require(doParallel)
require(plyr)
require(ShortRead)
require(doMC)
registerDoMC(detectCores())
getDoParWorkers()

bamFwd  <-  bam[strand(bam@first) == "-"]
bamRev  <-  bam[strand(bam@first) == "+"]

allLengthRev  <-  end(bamRev@last)-start(bamRev@first)+1
allLengthFwd  <-  end(bamFwd@first)-start(bamFwd@last)+1

allLength <- append(allLengthRev,allLengthFwd)

seqSize  <- as.numeric(bam@first@seqinfo@seqlengths)

allLengthSum <- as.data.frame(rev(sort(table(allLength))))
allLengthSum[,2]  <- as.double(row.names(allLengthSum))
allLengthSum[,3]  <- allLengthSum[,1]*allLengthSum[,2]
colnames(allLengthSum) <- c("FragCount", "FragLength", "Intensity")

#-----------------------
lBin <- seq(0,seqSize,binning)
allLengthSum <- cbind(allLengthSum, time_bin=cut(allLengthSum$FragLength, breaks=lBin)) 
allLengthSum <- ddply(allLengthSum, .(time_bin), summarize,
                      mFragLength = mean(FragLength),
                      sFragCount = sum(FragCount),
                      sIntensity = sum(Intensity), .parallel = TRUE) #Can be max or mean depending on point placement
#-----------------------

return(allLengthSum)
}